import json

from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.core.exceptions import ObjectDoesNotExist

from . import neural_feedback

net = neural_feedback.neural_network.NeuralNetwork()

request_example = '''{"method": "learn",
              "params": {
                  "complaint": [
                      "Регистрация \r\n6357200891000111 - 08 февраля 1997",
                      "default",
                      "iOS",
                      "2016-05-24 16:43",
                      "000023",
                      "i_svetochka_ur@mail.ru",
                      "",
                      "6357200891000111",
                      "16",
                      "08 февраля 1997",
                      "3.0.1.2",
                      "iPhone8,1",
                      "9.1",
                      "",
                      "",
                      "Старая версия IOS::Старая версия IOS<->Пациент не найден/проблемы полиса::Пациент не найден/Проблемы полиса",
                      "Проблемы",
                      "Проблемы Системы",
                      "Пациент не найден/Проблемы полиса"
                  ],
                  "category": "Пациент не найден/Проблемы полиса"
              }}'''


@api_view(['GET', 'POST'])
def dispatcher(request):
    request.body = json.loads(request.body.decode('utf-8'))

    if 'method' not in request.body:
        return Response("Please, specify method in request (analyze, learn).\nExample:\n" + request_example)

    if 'params' not in request.body or 'complaint' not in request.body['params']:
        return Response("Please, specify 'params' and 'complaint'.\nExample:\n" + request_example)

    if len(request.body['params']['complaint']) < 19:
        return Response("The complaint have unsupported format or contains not enough information.\nExample:\n"
                        + request_example)

    if request.body['method'] == 'analyze':
        complaint = request.body['params']['complaint']

        result = net(complaint)
        return Response(result)
    if request.body['method'] == 'learn':
        try:
            net.learn(request.body['params']['category'], request.body['params']['complaint'])
        except ObjectDoesNotExist:
            return Response('You can\'t learn the neural network by using this complaint, '
                            'because the network has no information about it.'
                            'It means, '
                            'that the network didn\'t analyze this complaint or it already learned on this complaint.')
        return Response('ok')
    else:
        return Response('Unknown method. Use "analyze" or "learn".\nExample:\n' + request_example)




