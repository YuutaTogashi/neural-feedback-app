# coding: utf8

import re
import pymorphy2


morph = pymorphy2.MorphAnalyzer()

ignored_parts = ['NPRO', 'PREP', 'CONJ', 'PRCL', 'INTJ']


def get_sum(obj):
    result = 0

    for key in obj:
        result += obj[key]
    return result


def catch(messages):
    word_dictionary = {}
    for message in messages:
        words = re.findall(r'\w+', message, re.UNICODE)

        for word in words:
            morph_word = morph.parse(word)[0]
            if morph_word.tag.POS in ignored_parts:
                continue

            if len(morph_word.normal_form) > 2:
                is_negative = ''
                if words[words.index(word) - 1] == 'не':
                    is_negative = 'не '

                if is_negative + morph_word.normal_form in word_dictionary:
                    word_dictionary[is_negative + morph_word.normal_form] += 1
                else:
                    word_dictionary[is_negative + morph_word.normal_form] = 1

    return word_dictionary



