# coding: utf8
from . import data
from . import word_catcher
from .. import models

from abc import ABCMeta, abstractmethod

fields = {
    "platform": 2,
    "prod_version": 10,
    "device": 11,
    "os_version": 12,
    "browser": 13
}


class Neuron(object):
    __metaclass__ = ABCMeta

    def __init__(self, category):
        self.category = category
        self.power = 0

    @abstractmethod
    def __call__(self, complaint):
        """Get category"""

    @abstractmethod
    def relearn(self, right_data):
        """Actions when that neuron was chosen, but wasn't right"""
    @abstractmethod
    def learn(self, complaint, is_chosen_one=True):
        """Actions when that neuron was chosen and was right """
    @abstractmethod
    def new_category(category, complaint):
        """Actions when that neuron was chosen and was right """


class CombineNeuron(Neuron):
    def __init__(self, category):
        super().__init__(category)
        self.fields_len = 1
        self.fields = fields

    def __call__(self, complaint):
        self.power = 0

        return self.get_power(complaint)

    def get_power(self, complaint):
        combined_values = ''
        for field in self.fields:
            combined_values += complaint[fields[field]]
        if combined_values in data.data_dict[self.category]['combined']:
            power = data.data_dict[self.category]['combined'][combined_values]

            return {combined_values: power}
        return {'unknown': 0}

    def learn(self, complaint, is_chosen_one=True):
        combined_values = ''
        for field in self.fields:
            combined_values += complaint[fields[field]]
        if combined_values not in data.data_dict[self.category]['combined']:
            data.data_dict[self.category]['combined'][combined_values] = 1
            self.fields_len += 1
        else:
            data.data_dict[self.category]['combined'][combined_values] += 1

    def relearn(self, right_data, current_data=None):
        if 'unknown' in right_data:
            return
        for key in right_data:
            if key == '__power__':
                continue
            data.data_dict[self.category]['combined'][key] -= 0.5
            if data.data_dict[self.category]['combined'][key] <= 0:
                del data.data_dict[self.category]['combined'][key]

    @staticmethod
    def new_category(category, complaint):
        if category not in data.data_dict:
            data.data_dict[category] = {}

        combined_values = ''
        for field in fields:
            combined_values += complaint[fields[field]]

        data.data_dict[category]['combined'] = {}
        data.data_dict[category]['combined'][combined_values] = 1


class MessageNeuron(Neuron):
    def __init__(self, category):
        super().__init__(category)
        self.fields_len = word_catcher.get_sum(data.data_dict[self.category]['words'])

    def __call__(self, complaint):
        self.power = 0
        self.powers = self.get_message_power(complaint[0])
        return self.powers

    def get_message_power(self, message):
        words = word_catcher.catch([message])

        result = {}
        for word in words:
            if word in data.data_dict[self.category]['words']:

                result[word] = data.data_dict[self.category]['words'][word]

        return result

    def relearn(self, bad_words, current_data=None):

        for word in bad_words:
            if word not in data.data_dict[self.category]['words']:
                continue

            data.data_dict[self.category]['words'][word] -= 1
            if self.fields_len > 1:
                self.fields_len -= 1
            if data.data_dict[self.category]['words'][word] <= 0:

                del data.data_dict[self.category]['words'][word]

    def learn(self, complaint, is_chosen_one=True):
        words = word_catcher.catch([complaint[0]])
        for word in words:
            if word not in data.data_dict[self.category]['words']:
                self.fields_len += 1
                data.data_dict[self.category]['words'][word] = 1
                continue
            self.fields_len += 1
            data.data_dict[self.category]['words'][word] += 1

    @staticmethod
    def new_category(category, message):
        if category not in data.data_dict:
            data.data_dict[category] = {}
        words = word_catcher.catch([message])

        data.data_dict[category]['words'] = {}
        for word in words:
            data.data_dict[category]['words'][word] = 1


class FieldsNeuron(Neuron):
    def __init__(self, category):
        super().__init__(category)

        self.fields = fields
        self.fields_len = len(fields)
        self.weights = {}
        if fields:
            for field in fields:
                self.weights[field] = 1

    def __call__(self, complaint):
        self.power = 0
        self.powers = {}
        for field in fields:
            if complaint[self.fields[field]] not in data.data_dict[self.category][field]:
                continue
            self.powers[field] = self.get_field_power(complaint, field) * self.weights[field]
            self.power += self.powers[field] * self.weights[field]

        return self.powers

    def get_field_power(self, complaint, field):
        # print(data.data_dict[self.category][field])
        field_sum = sum(data.data_dict[self.category][field].values())
        return data.data_dict[self.category][field][complaint[self.fields[field]]] / field_sum

    def relearn(self, right_powers, current_data=None):
        # powers = models.Complaint.objects.get(id=current_data[4]).data['field']

        for key in right_powers:
            if key == '__power__' or key not in self.powers:
                continue
            right_power = right_powers[key]

            if right_power <= self.powers[key] and self.weights[key] > 0.01:
                if current_data[self.fields[key]] in data.data_dict[self.category][key]:
                    if data.data_dict[self.category][key][current_data[self.fields[key]]] >= 2:
                        data.data_dict[self.category][key][current_data[self.fields[key]]] -= 0.25
                self.weights[key] -= 0.005

    def learn(self, complaint, is_chosen_one=True):
        for field in self.fields:
            if complaint[self.fields[field]] not in data.data_dict[self.category][field]:
                data.data_dict[self.category][field][complaint[self.fields[field]]] = 1
                continue
            data.data_dict[self.category][field][complaint[self.fields[field]]] += 1
        if not is_chosen_one:
            for weight in self.weights:
                if self.weights[weight] < 1:
                    self.weights[weight] += 0.01

    @staticmethod
    def new_category(category, complaint):
        if category not in data.data_dict:
            data.data_dict[category] = {}
        for field in fields:
            data.data_dict[category][field] = {}
            data.data_dict[category][field][complaint[fields[field]]] = 1


class JointNeuron:
    def __init__(self, neurons, exlusivity_coefficient=1):
        self.power = 0
        self.powers = {}
        self.exlusivity_coefficient = exlusivity_coefficient
        self.neurons = neurons
        self.main_category = ''
        
    def __call__(self, complaint):
        self.power = -1
        
        for neuron in self.neurons:
            self.powers[neuron] = self.neurons[neuron](complaint)

        for neuron in self.neurons:
            # print(self.powers)
            for item in self.powers[neuron]:
                is_one = True

                for tmp_neuron in self.neurons:
                    if tmp_neuron == neuron:
                        continue

                    if item in self.powers[tmp_neuron]:
                        is_one = False
                        break

                if is_one:
                    self.powers[neuron][item] *= self.exlusivity_coefficient

        for category in self.powers:
            self.powers[category]['__power__'] = 0
            if not self.main_category:
                self.main_category = category

            for item in self.powers[category]:
                if item == '__power__':
                    continue
                self.powers[category]['__power__'] += self.powers[category][item]

            self.powers[category]['__power__'] /= self.neurons[category].fields_len

            if self.power < self.powers[category]['__power__']:
                self.power = self.powers[category]['__power__']
                self.main_category = category

        return {'category': self.main_category,
                '__power__': self.power,
                '__powers__': self.powers[self.main_category]}

    def learn(self, right_category, complaint):
        print('********************' + complaint[4])
        complaint_saved = models.Complaint.objects.get(id=complaint[4])
        true_powers = complaint_saved.data
        chosen_category = complaint_saved.category
        print(right_category)
        if self.neurons[right_category] == self.neurons[chosen_category]:
            self.neurons[right_category].learn(complaint)
        else:
            self.neurons[right_category].learn(complaint, is_chosen_one=False)

        if self.main_category != right_category:
            self.neurons[chosen_category].relearn(self.powers[right_category], current_data=complaint)

    def append_neuron(self, category, neuron):
        self.neurons[category] = neuron
                
            
                
        


