import json
import os


path = os.path.dirname(os.path.abspath(__file__)) + '/data.json'
data_dict = json.loads(open(path, 'r').read())


def update(new_data):
    global data_dict
    data_dict = new_data
    open(path, 'w').write(json.dumps(data_dict))


def save():
    global data_dict
    open(path, 'w').write(json.dumps(data_dict))
