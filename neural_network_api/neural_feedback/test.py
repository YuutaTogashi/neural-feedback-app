# coding: utf8

import json
# import magic
import neural_network
import analyzer

import data
complaints = json.loads(open('dump.json', 'r').read())

net = neural_network.NeuralNetwork()
times = 0

result = 0

while result < 6000:
    result = 0
    inf = []
    all = 0
    categories_info = {}

    categories = []
    for complaint in complaints:
        if not analyzer.extract_category(complaint) or analyzer.extract_category(complaint) == 'То, что нужно удалить'\
                or analyzer.extract_category(complaint) == 'Категория3':
            continue

        net_category = net(complaint)
        true_category = analyzer.extract_category(complaint)

        if true_category not in categories_info:
            categories_info[true_category] = {}
            categories_info[true_category]['all'] = 1
            categories_info[true_category]['right'] = 0
        else:
            categories_info[true_category]['all'] += 1

        if net_category == true_category:
            categories_info[true_category]['right'] += 1

        # print('neuro: ' + net(complaint))
        # print('true: ' + analyzer.extract_category(complaint))
        # if
        if analyzer.extract_category(complaint) != net(complaint):
            pass
            # net.learn(complaint, analyzer.extract_category(complaint), right=False)
            # print('=((((((((((((((((((((((((((((((((((((((((((((((')
        else:
            # net.learn(complaint, analyzer.extract_category(complaint))
            result += 1
            # print('yup^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^^_^')
        all += 1
        net.learn(true_category, complaint)
        if all == 500:
            inf.append(result)
            continue

        if all % 500 == 0:
            inf.append(result - sum(inf))
        # if input():
        #     break

    print('inf: ', inf)
    print('result: ', result)
    print('all: ', all)
    times += 1
    print('times: ', times)

    for category in categories_info:
        print(category, ': ', categories_info[category]['right'], '/', categories_info[category]['all'],
              '(', categories_info[category]['right'] / categories_info[category]['all'], ')')


data.save()
