# coding: utf8

from . import data
from .. import models
from . import new_neuron


class NeuralNetwork:
    def __init__(self):
        self.categories = []
        for category in data.data_dict:
            self.categories.append(category)

        self.field_neurons = new_neuron.JointNeuron({}, exlusivity_coefficient=2)
        self.message_neurons = new_neuron.JointNeuron({}, exlusivity_coefficient=2)

        self.main_category = ''
        self.weigths = {}
        for category in self.categories:
            self.new_category(category)
            self.weigths[category] = {'message': {'right': 1, 'all': 1},
                                      'field': {'right': 1, 'all': 1}}

        self.stat = {}

    def __call__(self, complaint):
        self.possible_categories = {'field': self.field_neurons(complaint),
                                    'message': self.message_neurons(complaint)}

        tmp_power = 0
        for category in self.categories:
            category_power = 0
            for neuron_type in self.possible_categories:
                if category == self.possible_categories[neuron_type]['category']:
                    weigth = self.weigths[self.possible_categories[neuron_type]['category']][neuron_type]['right'] \
                             / self.weigths[self.possible_categories[neuron_type]['category']][neuron_type]['all']

                    category_power += weigth
            if category_power > tmp_power:
                tmp_power = category_power
                self.main_category = category

        base = models.Complaint.objects.create(
            category=self.main_category,
            id=complaint[4],
            data={'field': self.possible_categories['field'],
                  'message': self.possible_categories['message']})
        base.save()
        return self.main_category

    def new_category(self, category):
        field_neuron = new_neuron.FieldsNeuron(category)
        message_neuron = new_neuron.MessageNeuron(category)

        self.message_neurons.append_neuron(category, message_neuron)
        self.field_neurons.append_neuron(category, field_neuron)
        self.weigths[category] = {'message': {'right': 1, 'all': 1},
                                  'field': {'right': 1, 'all': 1}
                                  }

    def learn(self, right_category, complaint):
        if right_category not in self.categories:
            self.categories.append(right_category)

            new_neuron.FieldsNeuron.new_category(right_category, complaint)
            new_neuron.MessageNeuron.new_category(right_category, complaint[0])

            self.new_category(right_category)
            return
        self.field_neurons.learn(right_category, complaint)
        self.message_neurons.learn(right_category, complaint)

        for neuron_type in self.possible_categories:
            self.weigths[right_category][neuron_type]['all'] += 1
            if self.possible_categories[neuron_type]['category'] == right_category:

                self.weigths[right_category][neuron_type]['right'] += 1

        data.save()
        models.Complaint.objects.get(id=complaint[4]).delete()


