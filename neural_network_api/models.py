from django.db import models
from django.contrib.postgres.fields import JSONField


class Complaint(models.Model):
    data = JSONField()
    id = models.CharField(max_length=256, primary_key=True)
    category = models.CharField(max_length=256)


