from django.apps import AppConfig


class NeuralNetworkApiConfig(AppConfig):
    name = 'neural_network_api'
